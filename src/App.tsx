import React, { useEffect, useState } from 'react';
import './App.css';
import TopNav from "./components/TopNav/TopNav";

function App() {
  return (
    <>
      <TopNav />
    </>
  );
}

export default App;
