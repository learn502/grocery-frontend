export interface RespBodyStructure {
    data: any;
    statusCode: number;
    statusMessage: string;
}