const _constant: Record<string, string> = {}
/*---------API URLS-----------*/
_constant['GET_DISTINT_PRODUCTS_URL'] = `http://localhost:8086/api/v1/getItems`;
_constant['GET_DETAILS_URL'] = `http://localhost:8086/api/v1/itemDetails`
_constant['GET_MAXIMUM_PRICE_URL'] = `http://localhost:8086/api/v1/allMaxPricesPaginated`
export default _constant;