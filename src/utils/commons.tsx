export const convertDatetoYYYYMMHH = (date: Date): string => {
    var year = date.toLocaleString("default", { year: "numeric" });
    var month = date.toLocaleString("default", { month: "2-digit" });
    var day = date.toLocaleString("default", { day: "2-digit" });
    return year+month+day
} 

export const convertYYYYMMHHToDate = (dateString: string): string => {
    var year        = dateString.substring(0,4);
    var month       = dateString.substring(4,6);
    var day         = dateString.substring(6,8);
    const elements = [year, month, day]
    return elements.join('-')
}