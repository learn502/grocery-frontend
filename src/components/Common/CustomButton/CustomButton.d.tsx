export interface CustomButtonProps {
    disabled?:boolean;
    onClick: () => void;
}