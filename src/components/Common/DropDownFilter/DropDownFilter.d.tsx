export interface DropDownFilterProps {
  // label: string;
  placeholder: string;
  data: Array<string>;
  value: string;
  onChange: (e: any) => void;
}