import React, { useEffect, useState } from "react";
import CustomButton from "../Common/CustomButton/CustomButton";
import { Input, Loader, TextInput } from "@mantine/core";
import CONSTANTS from "../../utils/constants";
import DataTable from 'react-data-table-component';
import "./TableComponent.css";
import { get } from "../../utils/http";
import { convertYYYYMMHHToDate } from "../../utils/commons";
import { toast } from 'react-toastify';


const columns = [
  {
      name: 'ItemName',
      selector: (row: any) => row.itemName,
      width: '500px'
  },
  {
      name: 'datesk',
      cell: (row: any) => convertYYYYMMHHToDate(row.datesk.toString()),
      selector: (row: any) => row.datesk,
      width: '500px'
  },
  {
      name: 'Price (in Euros)',
      selector: (row: any) => row.price,
      width: '500px'
  }
];


function TableComponent() {
  const [buttonEnable, enableButton] = useState<boolean>(false);
  const [searchInput, setSearchInput] = useState<string>("");
  const [isLoading, setIsLoading]  = useState<boolean>(false);
  const [tableData, setTableData] = useState<any>(undefined);
  const [totalRows, setTotalRows] = useState<any>(undefined);

  useEffect(() => {
    fetchData(0, 500, "")
  }, [])

  const fetchData = async (page:any, size:any, item: any) => {
    setIsLoading(true)
    var options: any = {}
    var query: any = {}
    query.page = page
    query.size = size
    query.itemName = item
    options.query = query
    get(CONSTANTS.GET_MAXIMUM_PRICE_URL, options)
      .then((data: any) => {
        console.log(data)
        if(data?.response) {
          setTotalRows(data.page_details.total_elements)
          setTableData(data.response)
          toast.success('Fetched Data successfully');
        } else {
          toast.warn('No records')
        }
        
        setIsLoading(false)
      })
      .catch((error) => {
        console.log(error)
        toast.error(error?.statusMessage || 'Error');
        setIsLoading(false)
      })
  }

  function checkEnableButton(e: any) {
    if(e.target.value.length > 0) {
      enableButton(true)
    } else {
      enableButton(false)
    }
    setSearchInput(e.target.value)
  }


  function buttonClick() {
    fetchData(0, 500, searchInput)
  }

  return (
    <>
      {isLoading && <div className="loader">
          <Loader />
        </div>}
      {!isLoading && <div className="Search-btn">
        <TextInput placeholder="Search Item" onChange={(e) =>checkEnableButton(e)} value={searchInput} />
        <div className="button-div">
          <CustomButton disabled={!buttonEnable} onClick={buttonClick}>
            Search
          </CustomButton>
        </div>
      </div>}
      {!isLoading && tableData && <DataTable
          fixedHeader
          columns={columns}
          data={tableData}
          pagination
          paginationTotalRows={totalRows}
        />}
    </>
  );
}

export default TableComponent;
