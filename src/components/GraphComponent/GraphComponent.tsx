import React,{ useEffect, useState } from "react";
import CONSTANTS from "../../utils/constants";
import { get } from "../../utils/http";
import CustomButton from "../Common/CustomButton/CustomButton";
import CustomDatePicker from "../Common/CustomDatePicker/CustomDatePicker";
import DropDownFilter from "../Common/DropDownFilter/DropDownFilter";
import { Loader } from "@mantine/core";
import { convertDatetoYYYYMMHH, convertYYYYMMHHToDate } from "../../utils/commons";
import { Legend, Line, LineChart, Tooltip, XAxis, YAxis } from "recharts";
import CsvDownload from "react-json-to-csv";
import './GraphComponent.css';
import { toast } from 'react-toastify';

function GraphComponent(props: any) {
  const [item, setItem] = useState<string>("");
  const [itemList, setItemList] = useState<any>([]);

  const [fromDate, setFromDate] = useState<Date | undefined>(undefined);
  const [toDate, setToDate] = useState<Date | undefined>(undefined);

  //Button states
  const [buttonEnable, enableButton] = useState<boolean>(false);

  const [isLoading, setIsLoading] = useState<boolean>(false);

  const [graphData, setGraphData] = useState<any>(undefined);

  const [csvData, setCsvData] = useState<any>(undefined);

  const headers: String[] = ["itemName", "price", "datesk"];

  function checkButtonDisable() {
    if (item && fromDate && toDate) {
      enableButton(true);
    }
  }

  function fetchData() {
    getProductDetails();
  }

  useEffect(() => {
    checkButtonDisable();
  }, [item, fromDate, toDate]);

  function getProductsFromAPI() {
    setIsLoading(true);
    get(CONSTANTS.GET_DISTINT_PRODUCTS_URL)
      .then((data) => {
        setItemList(data);
        setIsLoading(false);
      })
      .catch((error) => {
        console.log(error);
        setIsLoading(false);
        toast.error("Error")
      });
  }

  function getProductDetails() {
    setIsLoading(true);
    var options: any = {};
    var query: any = {};
    query.itemName = item;
    query.from = convertDatetoYYYYMMHH(fromDate as Date);
    query.to = convertDatetoYYYYMMHH(toDate as Date);
    query.greaterThan = 0.0;
    options.query = query;
    get(CONSTANTS.GET_DETAILS_URL, options)
      .then((data) => {
        setIsLoading(false);
        setCsvData(data)
        setGraphData(convertResponseToGraphData(data))
        if(data.length == 0) {
          toast.warn("No records found for given range")
        } else {
        toast.success("Successfully fetched")
        }
      })
      .catch((error) => {
        console.log(error);
        setIsLoading(false);
        toast.error("Error")
      });
  }

  function checkClearToDate(d: Date) {
    if(toDate && toDate < d) {
        setToDate(undefined)
        enableButton(false)
    }
    setFromDate(d)
  }

  function convertResponseToGraphData(data: Array<any>): Array<any> {
    return data.map(item => {
            const obj: any = {}
            obj.price = item.price
            obj.date = convertYYYYMMHHToDate(item.datesk.toString()) 
            return obj
    })

}

  useEffect(() => {
    getProductsFromAPI();
  }, []);
  return (
    <div>
      {isLoading && <div className="loader">
          <Loader />
        </div>}
      {!isLoading && (
        <div>
          <div className="Search-Filter">
            <DropDownFilter
              value={item}
              data={itemList}
              placeholder="Pick Item"
              // label="Pick Item"
              onChange={setItem}
            />
            <CustomDatePicker
              value={fromDate}
              onChange={checkClearToDate}
              placeHolder="Select From Date"
            />
            <CustomDatePicker
              disabled={!fromDate}
              value={toDate}
              onChange={setToDate}
              minDate={fromDate}
              placeHolder="Select To Date"
            />
            <CustomButton disabled={!buttonEnable} onClick={fetchData}>
              Search
            </CustomButton>
          </div>
          <div className="nodata-txt">
            {!graphData && <>Please select Data from Filters</>}
          </div>
          {graphData && graphData.length > 0 && (
            <div className="graph-block">
              <CsvDownload className="download-btn" data={csvData}>DownLoad Report</CsvDownload>
              
            
                <LineChart className="graph-block" width={1000} height={800} data={graphData}>
                  <Line type="monotone" dataKey="price" stroke="#8884d8" />
                  <XAxis dataKey="date" />
                  <YAxis label={{ value: 'Price In Euros', angle: -90, position: 'insideLeft' }}/>
                  <Tooltip />
                  <Legend />
                </LineChart>
            </div>
          )}
          {graphData && graphData.length == 0 && (
            <p className="nodata-txt">Sorry no records found</p>
          )}
        </div>
      )}
    </div>
  );
}
export default GraphComponent;
